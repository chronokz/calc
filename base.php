<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Калькулятор имплантации зубов</title>
	<link rel="stylesheet" href="https://gelio-a.ru/wp-content/themes/dentum-wp/assets/css/main.css">
	<link rel="stylesheet" href="https://gelio-a.ru/wp-content/themes/dentum-wp/assets/less/add.css">
	<link rel="stylesheet" href="magnific-popup.css">
	<link rel="stylesheet" href="kalku.css">
	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.form.min.js"></script>
	<script src="jquery.magnific-popup.min.js"></script>
	<script src="owl.carousel.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" media="all"/>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src="postmessage.js"></script>
    <script src="Frame.js"></script>
</head>

<body>
	<div class="demo_calc_modal">
		<a href="calc.html" class="calc_modal"><i class="fa fa-clipboard" aria-hidden="true"></i> Open in modal</a>
	</div>


	<?php include 'calc.html'; ?>
</body>
</html>