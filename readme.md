#### Настройка Invisible reCaptcha
Создать аккаунт reCaptcha  и привязать к домену можно на этой странице:
https://www.google.com/recaptcha/admin#list
Указать полученный код можно на странице **calc.html** для кнопки .calculator__send

#### Размещение на странице
Вставить на страницу содержимое как в файле **calc.html**
Пример backend - в файле **index.php**
Пример frontend - в файле **base.php** 

#### Открытие модального окна по ссылке
Способ 1 (Размещение с помощью ссылки):
```
<a href="calc.html" class="calc_modal">Открыть калькулятор</a>
$('.calc_modal').magnificPopup({
    type: 'ajax',
    closeBtnInside: true
});
```
Способ 2 (Вызвать через javascript-callback)
```
$.magnificPopup.open({
    items: {
        src: 'calc.html',
        type: 'ajax'
    },
    closeBtnInside: true
});
```

#### Дополнительная информация по модальным окнам (включая API)
[http://dimsemenov.com/plugins/magnific-popup/documentation.html](http://dimsemenov.com/plugins/magnific-popup/documentation.html)

#### Ссылка на скачивание
[https://bitbucket.org/chronokz/calc/downloads/?tab=downloads](https://bitbucket.org/chronokz/calc/downloads/?tab=downloads)

git clone git@bitbucket.org:chronokz/calc.git

git clone https://chronokz@bitbucket.org/chronokz/calc.git

#### Ссылка на демонстрационную версию
http://html-dev.pro/gelio/

#### Пример работы калькулятора в режиме popup-окна
http://html-dev.pro/gelio/ (кликнуть по ссылке **Open in modal**)